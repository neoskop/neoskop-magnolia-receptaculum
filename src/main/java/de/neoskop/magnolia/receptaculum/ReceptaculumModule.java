package de.neoskop.magnolia.receptaculum;

import de.neoskop.magnolia.receptaculum.config.ReceptaculumModuleConfiguration;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;
import info.magnolia.module.blossom.module.BlossomModuleSupport;

public class ReceptaculumModule extends BlossomModuleSupport implements ModuleLifecycle {

  public void start(ModuleLifecycleContext moduleLifecycleContext) {
    if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_STARTUP) {
      super.initRootWebApplicationContext(ReceptaculumModuleConfiguration.class);
    }
  }

  public void stop(ModuleLifecycleContext moduleLifecycleContext) {
    if (moduleLifecycleContext.getPhase() == ModuleLifecycleContext.PHASE_SYSTEM_SHUTDOWN) {
      super.destroyDispatcherServlets();
      super.closeRootWebApplicationContext();
    }
  }
}
